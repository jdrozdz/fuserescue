#include "hospital.h"

Fuses fuses;

void initHospital(void){
	DDRB |= (1 << TARGET_VCC) | (1 << TARGET_RESET);
	DDRC |= (1 << START_BTM);
	DDRD |= 0x00;

	PORTB |= (1 << TARGET_RESET);
	PORTC |= (1 << START_BTM);
	PORTD |= 0x00;
}

void poll(void){
	_delay_ms(5);
}

void clockit(void){
	PORTD |= (1 << TARGET_SCI);
	_delay_us(250);
	PORTD &= ~(1 << TARGET_SCI);
}

uint8_t sendByte(uint8_t data, uint8_t instr){
	uint8_t   r,i;

	PORTD &= ~(1 << TARGET_SDI);
	PORTD &= ~(1 << TARGET_SII);

// First zero
	clockit();

	for(r=0, i=0; i<8; i++) {
	r <<= 1;
			r |= (PIND & (1 << TARGET_SDO)) != 0 ? 1:0;

	if(data & 0x80) PORTD |= (1 << TARGET_SDI);
	else PORTD &= ~(1 << TARGET_SDI);

	if(instr & 0x80) PORTD |= (1 << TARGET_SII);
			else PORTD &= ~(1 << TARGET_SII);

	data <<= 1;
	instr <<= 1;

	clockit();
}

	PORTD &= ~(1 << TARGET_SDI);
	PORTD &= ~(1 << TARGET_SII);

// Last 2 zero's
	clockit();
	clockit();

    return r;
}
void ChipErase(void) {
	sendByte(0x80, 0x4C);
	sendByte(0x00, 0x64);
	sendByte(0x00, 0x6C);
	poll();

	// Instruction NOP
	sendByte(0x00, 0x4C);
	poll();
}

void WriteFuseLowBits(uint8_t code) {
	sendByte(0x40, 0x4C);
	sendByte(code, 0x2C);
	sendByte(0x00, 0x64);
	sendByte(0x00, 0x6C);
	poll();
}

void WriteFuseHighBits(uint8_t code) {
	sendByte(0x40, 0x4C);
	sendByte(code, 0x2C);
	sendByte(0x00, 0x74);
	sendByte(0x00, 0x7C);
	poll();
}

void WriteFuseExtendedBits(uint8_t code) {
	sendByte(0x40, 0x4C);
	sendByte(code, 0x2C);
	sendByte(0x00, 0x66);
	sendByte(0x00, 0x6E);
	poll();
}

// Execute before while() loop!
void SetPatient(const char *patient){
	uint8_t patients_Co = (sizeof(Patients))/sizeof(char);
	uint8_t i, finds = 0;
	for(i = 0; i < patients_Co; i++){
		if(strcmp(Patients[i], patient)){
			finds++;
			if(strcmp(patient, "m8")){
				fuses.lfuse = 0xe1;
				fuses.hfuse = 0xd9;
			}else if(strcmp(patient, "m16")){
				fuses.lfuse = 0xc1;
				fuses.hfuse = 0x99;
			}else if(strcmp(patient, "m32")){
				fuses.lfuse = 0xe1;
				fuses.hfuse = 0x99;
			}else if(strcmp(patient, "t13a")){
				fuses.lfuse = 0x6a;
				fuses.hfuse = 0xff;
			}
		}
	}
// In future wheel be displayed error on UART communication port
	if(finds == 0 || finds > 1){
		exit(0);
	}
}

void healPatient(void){
	while (PINC & (1<<START_BTM));     // wait for button

	DDRD = (1<<TARGET_SDI)|(1<<TARGET_SII)|(1<<TARGET_SDO);
	PORTD = 0x00;

	_delay_ms(1);
	PORTB |= (1<<TARGET_VCC);
	_delay_us(40);
	PORTC &= ~(1<<TARGET_RESET);
	_delay_us(20);
	DDRD |= (1<<TARGET_SDI)|(1<<TARGET_SII)|(1<<TARGET_SCI);
	DDRD &= ~(0<<TARGET_SDO);

	_delay_us(500);

	ChipErase();
	WriteFuseLowBits(fuses.lfuse);
	WriteFuseHighBits(fuses.hfuse);
//              WriteFuseExtendedBits(EFUSE);

	_delay_ms(10);
	PORTC |= (1<<TARGET_RESET);
	PORTB &= ~(1<<TARGET_VCC);
	_delay_ms(250);
}
