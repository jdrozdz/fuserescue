/*
 * hospital.h
 *
 *  Created on: 24 lis 2014
 *      Author: jerzyk
 */

#ifndef EMERG_HOSPITAL_H_
#define EMERG_HOSPITAL_H_

#include <avr/io.h>
#include <util/delay.h>
#include <string.h>

// ATMEGA32/32A - 1Mhz -U lfuse:w:0xe1:m -U hfuse:w:0x99:m
// ATMEGA 8 - 8Mhz -U lfuse:w:0xe4:m -U hfuse:w:0xd9:m
// ATMEGA 8/8A - 1Mhz -U lfuse:w:0xe1:m -U hfuse:w:0xd9:m
// ATMEGA16/16A - 1Mhz -U lfuse:w:0xc1:m -U hfuse:w:0x99:m
// ATTINY13A - 1Mhz -U lfuse:w:0x6a:m -U hfuse:w:0xff:m -U efuse:w:0xff:m

typedef struct {
	uint8_t lfuse = 0xe1;
	uint8_t hfuse = 0x99;
	uint8_t efuse;
} Fuses;

extern char *Patients[] = { "m32", "m8", "m16", "t13a" };

extern Fuses fuses;

#define TARGET_RESET PB1
#define TARGET_VCC PB0
#define TARGET_SCI PD3
#define TARGET_SDO PD2
#define TARGET_SII PD1
#define TARGET_SDI PD0

#define RDY_LED PB2
#define START_BTM PC1

void initHospital(void);
void poll(void);
void clockit(void);
void ChipErase(void);
void WriteFuseLowBits(uint8_t code);
void WriteFuseHighBits(uint8_t code);
void WriteFuseExtendedBits(uint8_t code);
void SetPatient(uint8_t patient);
void healPatient(void);

#endif /* EMERG_HOSPITAL_H_ */
